## webdon-Sass

These are the Sass files for webdon.

### Usage

To use, link into a project and make sure you have Bourbon and Neat in place already.


### Pre-built classes

#### Grid

A couple of classes are predefined to make building a grid easier:

* `.row` - an outer row of columns.
* `.span-X` - a container spanning a number of columns, where X is a number between 1 and 12.

So you can whip up a grid easily with something like this:

```html
<div class="row">
  <div class="span-8">
    Some content.
  </div>
  <div class="span-4">
    Some other content.
  </div>
</div>
```

#### Video wrapper

Videos can be wrapped to fill their grid container. To wrap a 16:9 video you'd do:

```html
<div class="video">
  <div class="wrap-16-9">
    <iframe src="http://www.youtube.com..."></iframe>
  </div>
</div>
```

And a 4:3 video would be wrapped like so:

```html
<div class="video">
  <div class="wrap-4-3">
    <iframe src="http://www.youtube.com..."></iframe>
  </div>
</div>
```
